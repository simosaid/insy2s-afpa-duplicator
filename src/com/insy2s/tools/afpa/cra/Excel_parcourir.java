package com.insy2s.tools.afpa.cra;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_parcourir {
	public static void main(String[] args) {
		changeName(new File("C:\\Users\\saidm\\OneDrive\\Bureau\\DWWM\\Suivi\\ANNEXES 3\\toto.xlsx"), "SAID Mohamed", "F5");
	}

	@SuppressWarnings("resource")
	public static void changeName(File file, String name, String cellName) {
		try {
			FileInputStream in = new FileInputStream(file);
			// cr�er une instance workbook qui fait r�f�rence au fichier xlsx
			XSSFWorkbook wb = new XSSFWorkbook(in);
			XSSFSheet sheet = wb.getSheetAt(0);
			CellReference cr = new CellReference(cellName);
			Cell cell = sheet.getRow(cr.getRow()).getCell(cr.getCol());
			System.out.println(cell.getAddress().formatAsString());
			System.out.println(cell.getStringCellValue());
			cell.setCellValue("Nom  et pr�nom : " + name);
			FileOutputStream out = new FileOutputStream(file);
			wb.write(out);
			out.close();
			in.close();
			System.out.println("Le fichier excel " + file.getName() + " a �t� modifi� avec succ�s");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}