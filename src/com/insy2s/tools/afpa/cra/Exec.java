package com.insy2s.tools.afpa.cra;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Exec {
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();

		System.out.println(conf);

		File craFile = new File("./data/" + conf.getCraFileName());
		System.out.println("chargement fichier cra :");
		if (!craFile.exists()) {
			System.err.println("... ko");
			System.err.println("mettez le fichier " + conf.getCraFileName() + " dans le repertoire data !!");
			return;
		} else {
			System.out.println("... ok");
		}
		String ext = conf.getCraFileName().substring(conf.getCraFileName().lastIndexOf('.'));

		File studentsFile = new File("./data/" + conf.getStudentsFileName());
		System.out.println("chargement fichier liste des stagiaires :");
		if (!studentsFile.exists()) {
			System.err.println("... ko");
			System.err.println("mettez le fichier " + conf.getStudentsFileName() + " dans le repertoire data !!");
			return;
		} else {
			System.out.println("... ok");
		}

		BufferedReader brStudents = new BufferedReader(new FileReader(studentsFile));
		brStudents.lines().forEach(ligne -> {
			String studentname = ligne;  
			System.out.println("cr�ation de copie pour " + ligne);
			ligne = conf.getFileNamePrefix() + conf.getSeparator() + ligne;
			if (conf.getFileNameSuffix().trim().length() != 0) {
				ligne += conf.getSeparator() + conf.getFileNameSuffix();
			}
			String fileDestName = ligne.trim().replaceAll("\\s", conf.getSpaceReplacer());
			File dest = new File("./out/" + fileDestName + ext);
			try {
				if (dest.exists()) {
					System.err.println("... ko");
					System.err.println("attention le fichier " + dest.getCanonicalPath() + " existe deja !!");
				} else {
					Files.copy(Paths.get(craFile.toURI()), new FileOutputStream(dest));
					if (conf.getCellName() != null) {
						ExcelProcessor.changeName(dest,studentname, conf.getCellName());
					}
					System.out.println("... ok");
				}
			} catch (Exception e) {
				System.err.println("... ko");
				System.err.println(e.getMessage());
			}
		});
		brStudents.close();
	}
}
