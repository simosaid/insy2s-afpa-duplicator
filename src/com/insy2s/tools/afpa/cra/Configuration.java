package com.insy2s.tools.afpa.cra;

import java.io.FileInputStream;
import java.util.Properties;

public class Configuration {
	private final String cellName;
	private final String fileNamePrefix;
	private final String fileNameSuffix;
	private final String spaceReplacer;
	private final String separator;
	private final String craFileName;
	private final String studentsFileName;
	
	public Configuration() throws Exception {
		Properties appProps = new Properties();
		appProps.load(new FileInputStream("./conf/app.properties"));
		this.fileNamePrefix = appProps.getProperty("file.name.prefix","CDA-20079");
		this.fileNameSuffix = appProps.getProperty("file.name.suffix","");
		this.cellName = appProps.getProperty("cell.name",null);;
		this.spaceReplacer = appProps.getProperty("space.replacer","-");
		this.separator = appProps.getProperty("separator","-");
		this.craFileName = appProps.getProperty("cra.file.name","AFPA_Rapport_Hebdo.xlsx");
		this.studentsFileName = appProps.getProperty("students.file.name","students.txt");
	}

	public String getFileNamePrefix() {
		return fileNamePrefix;
	}

	public String getFileNameSuffix() {
		return fileNameSuffix;
	}

	public String getSpaceReplacer() {
		return spaceReplacer;
	}

	public String getSeparator() {
		return separator;
	}

	public String getCraFileName() {
		return craFileName;
	}

	public String getStudentsFileName() {
		return studentsFileName;
	}

	@Override
	public String toString() {
		return "Configuration [\n\tfileNamePrefix=" + fileNamePrefix + "\n\tfileNameSuffix=" + fileNameSuffix
				+ ",\n\tspaceReplacer=" + spaceReplacer + ",\n\tseparator=" + separator + ",\n\tcraFileName=" + craFileName
				+ ",\n\tstudentsFileName=" + studentsFileName + "\n]";
	}

	public String getCellName() {
		return cellName;
	}
}
